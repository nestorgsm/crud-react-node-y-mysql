import BlogModel from "../models/BlogModel.js"

/* METODOS PARA EL CRUD */

//Mostrar todos los registros
export const getAllBlogs = async (req, res) => {
    try{
        const blogs = await BlogModel.findAll()
        res.json(blogs)
    } catch(error) {
        res.json({message: error.message})
    }
}

//Mostrar un solo registro
export const getBlog = async (req, res) => {
    try{
        const blog = await BlogModel.findAll({
            where: {
                id: req.params.id
            }
        })
        //res,json(blog) //Asi me retorna un arreglo con el jsoon en la posicion 0
        res.json(blog[0]) //retorna unicamente el contenido de la posicion 0 la cual es un json
    } catch(error) {
        res.json({message: error.message})
    }
}

//CREAR UN BLOG
export const createBlog = async(req, res) => {
    try {
        await BlogModel.create(req.body)
        res.json({
            "message": "Registro creado correctamente",
            "status": true
        })
    } catch (error) {
        res.json({
            "message": error.message,
            "status": false
        })
    }
}

//actualiza run registro
export const updateBlog = async(req, res) => {
    try {
        await BlogModel.update(req.body ,{
            where: {
                id: req.params.id
            }
        })
        res.json({
            "message": "Registro actualizado correctamente",
            "status": true
        })
    } catch (error) {
        res.json({
            "message": error.message,
            "status": false
        })
    }
}

//eliminar un registro
export const deleteBlog = async(req, res) => {
    try {
        await BlogModel.destroy({
            where: {
                id:req.params.id
            }
        })
        res.json({
            "message": "Registro borrado correctamente",
            "status": true
        })
    } catch (error) {
        res.json({
            "message": error.message,
            "status": false
        })
    }
}