import {Sequelize} from 'sequelize'
import 'dotenv/config'

const db = new Sequelize(process.env.DB, process.env.USER, process.env.PASSWORD, {
    host: process.env.HOST,
    dialect: 'mysql'
});

export default db