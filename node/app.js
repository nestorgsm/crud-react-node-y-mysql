import  express  from "express"
import cors from 'cors'
import db from './database/db.js'
//importar enrutador
import blogRoutes from './routes/routes.js'

const app = express()

app.use(cors())
app.use(express.json())
app.use('/blogs', blogRoutes)

try {
    await db.authenticate()
    console.log("conexion exitosa a la bd")
} catch (error) {
    console.log(`Error conexion bd: ${error}`)
}

app.get('/', (req, res) => {
    res.send('hola mundo')
})

app.listen('4000', ()=> {
    console.log('Server running in http://localhost:4000/')
})