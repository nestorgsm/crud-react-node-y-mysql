import axios from 'axios'
import {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'

const URI = 'http://localhost:4000/blogs/'

const CompShowBlogs = () => {

    //blogs es un valor, setBlog es una funcion para actualizarlo - HOOKS
    const [blogs, setBlog] = useState([])

    /*useEffect es utilizar un "efecto" donde un "efecto" hace referencia a algo que react no controla*/
    useEffect( ()=>{
        getBlogs()
    }, [])

    //procedimiento para mostrar todos los blogs
    const getBlogs = async () => {
        const res = await axios.get(URI)
        setBlog(res.data)
    }

    //procedimiento para eliminar un blog
    const deleteBlog = async (id) => {
        await axios.delete(`${URI}${id}`)
        getBlogs()
    }

    return(
    <div className='container'>
        <div className='row'>
            <div className='col'>
                <Link to="/create" className='btn btn-md btn-primary mt-2 mb-2'>Crear <i className="fa-solid fa-circle-plus"></i></Link>
            <table className="table">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Titulo</th>
                    <th scope="col">Contenido</th>
                    <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        blogs.map ( (blog)=> (
                            <tr key={blog.id}>
                                <td>{blog.title}</td>
                                <td>{blog.content}</td>
                                <td>
                                     <Link to={`/edit/${blog.id}`} className="btn btn-info btn-sm" > Editar <i className="fa-solid fa-square-pen"></i></Link>   
                                    <button onClick={()=>deleteBlog(blog.id)} className='btn btn-danger btn-sm'>Eliminar <i className='fa-solid fa-circle-minus'></i></button>
                                </td>
                            </tr>
                        ))
                    }

                </tbody>
                </table>
            </div>
        </div>
    </div>
    )

}

export default CompShowBlogs