import axios from 'axios'
import {useState} from 'react'
import {useNavigate} from 'react-router-dom'

const URI = 'http://localhost:4000/blogs/'

const CompCreateBlog = () => {

    const [title, setTitle] = useState('')
    const [content, setContent] = useState('')
    const navigate = useNavigate()

    //procedimiento para guardar
    const store = async(e) => {
        e.preventDefault()
        await axios.post(URI, {
            title, content
        })
        navigate('/')
    }

    return (
        <div>
            <h1>Crear Blog</h1>
            <form onSubmit={store}>
                <div className='mb-3 '>
                    <label className='form-label'>Titulo</label>
                    <input type="text" className='form-control' value={title} onChange={ (e)=> setTitle(e.target.value)}></input>
                    
                </div>
                <div className='mb-3 '>
                    <label className='form-label'>Contenido</label><br/>
                    <textarea value={content} onChange={ (e)=> setContent(e.target.value)} className="form-control">
                    </textarea>
                </div>
                <button type='submit' className='btn btn-sm btn-primary'>Guardar</button>
            </form>
        </div>
    )
}

export default CompCreateBlog