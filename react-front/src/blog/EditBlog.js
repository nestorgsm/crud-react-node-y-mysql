import axios from 'axios'
import {useState, useEffect} from 'react'
import {useNavigate, useParams} from 'react-router-dom'

const URI = 'http://localhost:4000/blogs/'

const CompEditBlog = () => {

    const [title, setTitle] = useState('')
    const [content, setContent] = useState('')
    const navigate = useNavigate()

    const {id} = useParams()

    //actualizar
    const update = async(e) => {
        e.preventDefault()
        await axios.put(URI+id, {
            title: title, 
            content: content
        })
        navigate('/')
    }

    useEffect ( ()=> {
        getBlogById()
    },[] )

    const getBlogById = async() => {
        const res = await axios.get(URI+id)
        setTitle(res.data.title)
        setContent(res.data.content)
    }

    return (
        <div>
            <h1>Editar Blog</h1>
            <form onSubmit={update}>
                <div className='mb-3 '>
                    <label className='form-label'>Titulo</label>
                    <input type="text" className='form-control' value={title} onChange={ (e)=> setTitle(e.target.value)}></input>
                    
                </div>
                <div className='mb-3 '>
                    <label className='form-label'>Contenido</label><br/>
                    <textarea value={content} onChange={ (e)=> setContent(e.target.value)} className="form-control">
                    </textarea>
                </div>
                <button type='submit' className='btn btn-sm btn-primary'>Guardar</button>
            </form>
        </div>
    )

}

export default CompEditBlog