import logo from './logo.svg';
import './App.css';

//importamos componentes
import CompShowBlogs from './blog/ShowBlogs';
import CompCreateBlog from './blog/CreateBlogs';
import CompEditBlog from './blog/EditBlog';

import { BrowserRouter, Route, Routes } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />


        {/* Se utliza la etiqueta className porque la etiqueta class es reservada por parte 
        de react, para acceder a los estilos ocupamos la clase de bootstrap por eso el className */}
        {/* <button className='btn btn-sm btn-success'>Boton de ejemplo <i className="fa-brands fa-react"></i></button> */}
      </header>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<CompShowBlogs/>}> </Route>
          <Route path='/create' element={<CompCreateBlog/>} > </Route>
          <Route path='/edit/:id' element={<CompEditBlog/>} > </Route>
        </Routes>
      </BrowserRouter>
      {/* <CompShowBlogs></CompShowBlogs> */}
    </div>
  );
}

export default App;
